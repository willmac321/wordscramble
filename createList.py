#!/usr/bin/env python3

import sys
import random

def createList(word, s, x, y):
    #size of array
    x, y = int(x), int(y)
    #set the seed, allow to be input to decrypt
    random.seed(int(s))

    #check if word can fit
    if(x <= len(word)):
        print('enlarging array, not enough room for word')
        x = len(word) + 1
    if(y <= len(word)):
        print('enlarging array, not enough room for word')
        y = len(word) + 1

    wSpot = random.randint(0, x - 1 - len(word))
    #tracker for insert
    o = 0

    arr = [[0] * x for i in range(y)]
    for i in range(x):
        for j in range(y):
            #if x index is = to insert spot, start insert
            if(i == wSpot + o and o < len(word)):
                arr[i][j] = word[o]                     
                o += 1
            else:
                arr[i][j] = chr(random.randint(65, 122))

    return arr, x, y

def rotate(arr, x, y):
    #rotate the array
    narr = [[0] * y for i in range(x)] 
    for i in range(x):
        for j in range(y):
            narr[j][i] = arr[i][j] 

    return narr

def shuffle(arr, x, y):
    #shuffle a letter in array a direction
    shufx, shufy = random.randint(1, x - 2), random.randint(1, y - 2)
    #swap direcition 0 is up go counterclockwise
    swapDir = random.randint(0, 4)

    if(swapDir == 0):
        temp = arr[shufx][shufy] 
        arr[shufx][shufy] = arr[shufx][shufy - 1]
        arr[shufx][shufy - 1] = temp 
    elif(swapDir == 1):
        temp = arr[shufx][shufy] 
        arr[shufx][shufy] = arr[shufx + 1][shufy]
        arr[shufx + 1][shufy] = temp 
    elif(swapDir == 2):
        temp = arr[shufx][shufy] 
        arr[shufx][shufy] = arr[shufx][shufy + 1]
        arr[shufx][shufy + 1] = temp 
    elif(swapDir == 3):
        temp = arr[shufx][shufy] 
        arr[shufx][shufy] = arr[shufx - 1][shufy]
        arr[shufx - 1][shufy] = temp 

    arr = rotate(arr, x, y)
    
    return arr

def printRows(arr):
    for x in arr:
        print(x)

x = input('please input array length \n')
y = x
seed = input('input seed integer \n')
word = input("please supply phrase to hide\n")
arr, x, y = createList(word, seed, x, y)
for i in range(random.randint(50, 100)):
    arr = shuffle(arr, x, y)
printRows(arr)
